# reverseshell

Technologies Used:
Python 3.10.2

#Requirements
Python 3.x or greater


## Description
Basic reverse shell that allows you to run commands in a terminal/CMD on another PC. 

## Visuals
On the client side, it will prompt you for an IP address, of course you can change the code how ever you'd like to remove the prompt.


![Inline image](client1.png)



A similar prompt pops-up on the server side, remember that both the client and the server need the IP address of the server. So the IP address of the machine that is running your server script is what is needed for both prompts.


![Inline image](server1.png)



Once the server and the client are provided the IP address, we run a sample command, and it returns the current working directory. In ubuntu the command is "pwd" as you can see below.



![Inline image](server2.png)
![Inline image](client2.png)
## Installation
Please ensure that you have Python 3.0 or greater installed. Once you have met that requirement

## Usage
This software should only be used on systems that you have been given the explicit authority to do so. Using this software to compromise systems without permission of the systems owner is against the law and not the purpose of this software. 


## Contributing
If you spot a problem with the docs, search if an issue already exists. If a related issue doesn't exist, you can open a new issue using a relevant issue form.

Scan through our existing issues to find one that interests you. As a general rule, we don’t assign issues to anyone. If you find an issue to work on, you are welcome to open a PR with a fix.

When reporting an issue, it is important to state which version of Python you are using, and which operating systems you are using for the server and the client.

## Authors and acknowledgment
Benjamin Morales Perez - Owner


## Project status
Currently maintained by current owner.
